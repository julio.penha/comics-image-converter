echo "the comics with webp images will be converted to png images"
i=0
for comic in *.cbz; do
	unzip -o "${comic}"
	mv "$comic" "${comic%.cbz}.old"
	for file in *.webp; do
		convert "$file" "${file%.webp}.png"
		printf 'converting image %s\n' "$file"
		((i++))
	done
	echo "all images are converted to png now"

	echo "creating new .cbz files now"
	zip "${comic}" *.png

	echo "removing temporary files"
	rm *.png
	rm *.webp
	rm *.txt
done

echo "all comics images are converted to png now!"